SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[EDI_GET_STATE] 
(
	@BRANCH int
)
RETURNS VARCHAR(3)
AS
BEGIN

	DECLARE @STATE AS VARCHAR(3);


	SELECT @STATE = CASE 
						WHEN @BRANCH = 1 THEN 'SA'
						WHEN @BRANCH = 2 THEN 'QLD'
						WHEN @BRANCH = 3 THEN 'QLD'
						WHEN @BRANCH = 4 THEN 'VIC'
						WHEN @BRANCH = 5 THEN 'NSW'
						WHEN @BRANCH = 6 THEN 'WA'
						WHEN @BRANCH = 9 THEN 'NSW'
					ELSE 
						'' 
					END
	RETURN @STATE;

END
GO
