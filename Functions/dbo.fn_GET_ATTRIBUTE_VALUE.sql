SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION fn_GET_ATTRIBUTE_VALUE 
(
	@JSON NVARCHAR(MAX)

)

RETURNS @tbl_Attribute TABLE(Attribute VARCHAR(50), AttributeValue VARCHAR(100))

AS
BEGIN
	DECLARE @Attribute VARCHAR(50) = NULL;
	DECLARE @AttributeValue VARCHAR(100) = NULL;
	DECLARE @WorkflowType VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.workflow_type'));
		DECLARE @Outcome VARCHAR(50) = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.outcome'));

	IF @WorkflowType = 'deliver' AND @Outcome = 'delivered'
	BEGIN
		IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.label')) = 'Signature'
		BEGIN
			SET @Attribute = 'Signature';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image.id'));
		END
		ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].label')) = 'ATL Photo' AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NULL
		BEGIN
			SET @Attribute = 'ATL With Photo';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].id'));
		END
		ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.signature_image')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.attachments[0].label')) IS NULL AND (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NULL
		BEGIN
			SET @Attribute = 'ATL Without Photo';
			SET @AttributeValue = NULL;
		END
		ELSE IF (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code')) IS NOT NULL 
		BEGIN
			SET @Attribute = 'DLB';
			SET @AttributeValue = (SELECT JSON_VALUE(@JSON, '$.message_data.workflow_completion.evidence.custom_data.location_code'));
		END
	END

	INSERT INTO @tbl_Attribute VALUES (@Attribute, @AttributeValue);
	RETURN 
END
GO
