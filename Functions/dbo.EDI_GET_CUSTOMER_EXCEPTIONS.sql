SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[EDI_GET_CUSTOMER_EXCEPTIONS] 
(
	@AccountNumber varchar(20)
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @ExceptionCount AS VARCHAR(10);

	SET @ExceptionCount = 'True'


/****** Script for SelectTopNRows command from SSMS  ******/
Select @ExceptionCount = Case 

(SELECT COUNT(*) 
--TOP (1000) [Sno]
--      ,[AccountNumber]
--      ,[AccountName]
--      ,[MailTemplateid]
--      ,[CreatedBy]
--      ,[CreatedDatetime]
--      ,[EditedBy]
--      ,[EditedDatetime]
--      ,[isactive]
  FROM [dbo].[CustomerExceptions] where AccountNumber = @AccountNumber and MailTemplateid = '1' and isactive = 1)
  when 1 then 'false' else 'true' end
	RETURN @ExceptionCount;

END
GO
