SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_consignment] (
		[cd_id]                                       [int] NOT NULL,
		[cd_company_id]                               [int] NOT NULL,
		[cd_account]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                                 [int] NULL,
		[cd_import_id]                                [int] NULL,
		[cd_ogm_id]                                   [int] NULL,
		[cd_manifest_id]                              [int] NULL,
		[cd_connote]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                                     [smalldatetime] NOT NULL,
		[cd_consignment_date]                         [smalldatetime] NULL,
		[cd_eta_date]                                 [smalldatetime] NULL,
		[cd_eta_earliest]                             [smalldatetime] NULL,
		[cd_customer_eta]                             [smalldatetime] NULL,
		[cd_pickup_addr0]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                            [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]                          [int] NULL,
		[cd_pickup_record_no]                         [int] NULL,
		[cd_pickup_confidence]                        [int] NULL,
		[cd_pickup_contact]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]                          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]                        [int] NULL,
		[cd_delivery_record_no]                       [int] NULL,
		[cd_delivery_confidence]                      [int] NULL,
		[cd_delivery_contact]                         [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]                     [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                             [int] NULL,
		[cd_stats_depot]                              [int] NULL,
		[cd_pickup_branch]                            [int] NULL,
		[cd_pickup_pay_branch]                        [int] NULL,
		[cd_deliver_branch]                           [int] NULL,
		[cd_deliver_pay_branch]                       [int] NULL,
		[cd_special_driver]                           [int] NULL,
		[cd_pickup_revenue]                           [float] NULL,
		[cd_deliver_revenue]                          [float] NULL,
		[cd_pickup_billing]                           [float] NULL,
		[cd_deliver_billing]                          [float] NULL,
		[cd_pickup_charge]                            [float] NULL,
		[cd_pickup_charge_actual]                     [float] NULL,
		[cd_deliver_charge]                           [float] NULL,
		[cd_deliver_payment_actual]                   [float] NULL,
		[cd_pickup_payment]                           [float] NULL,
		[cd_pickup_payment_actual]                    [float] NULL,
		[cd_deliver_payment]                          [float] NULL,
		[cd_deliver_charge_actual]                    [float] NULL,
		[cd_special_payment]                          [float] NULL,
		[cd_insurance_billing]                        [float] NULL,
		[cd_items]                                    [int] NULL,
		[cd_coupons]                                  [int] NULL,
		[cd_references]                               [int] NULL,
		[cd_rating_id]                                [int] NULL,
		[cd_chargeunits]                              [int] NULL,
		[cd_deadweight]                               [float] NULL,
		[cd_dimension0]                               [float] NULL,
		[cd_dimension1]                               [float] NULL,
		[cd_dimension2]                               [float] NULL,
		[cd_volume]                                   [float] NULL,
		[cd_volume_automatic]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]                        [float] NULL,
		[cd_import_volume]                            [float] NULL,
		[cd_measured_deadweight]                      [float] NULL,
		[cd_measured_volume]                          [float] NULL,
		[cd_billing_id]                               [int] NULL,
		[cd_billing_date]                             [smalldatetime] NULL,
		[cd_export_id]                                [int] NULL,
		[cd_export2_id]                               [int] NULL,
		[cd_pickup_pay_date]                          [smalldatetime] NULL,
		[cd_delivery_pay_date]                        [smalldatetime] NULL,
		[cd_transfer_pay_date]                        [smalldatetime] NULL,
		[cd_activity_stamp]                           [datetime] NULL,
		[cd_activity_driver]                          [int] NULL,
		[cd_pickup_stamp]                             [datetime] NULL,
		[cd_pickup_driver]                            [int] NULL,
		[cd_pickup_pay_driver]                        [int] NULL,
		[cd_pickup_count]                             [int] NULL,
		[cd_pickup_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                             [datetime] NULL,
		[cd_accept_driver]                            [int] NULL,
		[cd_accept_count]                             [int] NULL,
		[cd_accept_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                            [datetime] NULL,
		[cd_indepot_driver]                           [int] NULL,
		[cd_indepot_count]                            [int] NULL,
		[cd_transfer_notified]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                             [datetime] NULL,
		[cd_failed_driver]                            [int] NULL,
		[cd_deliver_stamp]                            [datetime] NULL,
		[cd_deliver_driver]                           [int] NULL,
		[cd_deliver_pay_driver]                       [int] NULL,
		[cd_deliver_count]                            [int] NULL,
		[cd_deliver_pod]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                            [datetime] NULL,
		[cd_pricecode]                                [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                             [int] NULL,
		[cd_delivery_agent]                           [int] NULL,
		[cd_agent_pod]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                           [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]                          [datetime] NULL,
		[cd_agent_pod_entry]                          [datetime] NULL,
		[cd_completed]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]                          [datetime] NULL,
		[cd_cancelled_by]                             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                                    [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                           [datetime] NULL,
		[cd_transfer_driver]                          [int] NULL,
		[cd_transfer_count]                           [int] NULL,
		[cd_transfer_to]                              [int] NULL,
		[cd_toagent_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                            [datetime] NULL,
		[cd_toagent_driver]                           [int] NULL,
		[cd_toagent_count]                            [int] NULL,
		[cd_toagent_name]                             [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                            [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                                [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                              [int] NULL,
		[cd_last_stamp]                               [datetime] NULL,
		[cd_last_count]                               [int] NULL,
		[cd_accept_driver_branch]                     [int] NULL,
		[cd_activity_driver_branch]                   [int] NULL,
		[cd_deliver_driver_branch]                    [int] NULL,
		[cd_deliver_pay_driver_branch]                [int] NULL,
		[cd_failed_driver_branch]                     [int] NULL,
		[cd_indepot_driver_branch]                    [int] NULL,
		[cd_last_driver_branch]                       [int] NULL,
		[cd_pickup_driver_branch]                     [int] NULL,
		[cd_pickup_pay_driver_branch]                 [int] NULL,
		[cd_special_driver_branch]                    [int] NULL,
		[cd_toagent_driver_branch]                    [int] NULL,
		[cd_transfer_driver_branch]                   [int] NULL,
		[cd_pickup_state_name]                        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr0]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr1]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr2]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr3]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_email]                       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_suburb]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_state_name]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_postcode]                    [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_country]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact]                     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact_phone]               [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]                [datetime] NULL,
		[cd_lookup_created_stamp]                     [datetime] NOT NULL,
		[cd_lookup_updated_stamp]                     [datetime] NOT NULL,
		[cd_locked_for_editing_after_redirection]     [bit] NOT NULL,
		[cd_locked_for_editing_after_redelivery]      [bit] NOT NULL,
		CONSTRAINT [PK_edi_consignment]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [cd_date])
	WITH (STATISTICS_NORECOMPUTE = ON)
)
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__000AF8CF]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__00FF1D08]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__01F34141]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__02E7657A]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__03DB89B3]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__04CFADEC]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__05C3D225]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__06B7F65E]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__07AC1A97]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__08A03ED0]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__09946309]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0A888742]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__0B7CAB7B]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__0C70CFB4]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__0D64F3ED]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__0E591826]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__0F4D3C5F]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__10416098]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__113584D1]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__1229A90A]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__131DCD43]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__1411F17C]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__150615B5]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__15FA39EE]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__16EE5E27]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__17E28260]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__18D6A699]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__19CACAD2]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1ABEEF0B]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__1BB31344]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__1CA7377D]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__1D9B5BB6]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__1E8F7FEF]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__1F83A428]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_it__2077C861]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__216BEC9A]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__226010D3]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ra__2354350C]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ch__24485945]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__253C7D7E]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__2630A1B7]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__2724C5F0]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__2818EA29]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__290D0E62]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__2A01329B]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__2AF556D4]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__2BE97B0D]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__2CDD9F46]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__2DD1C37F]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__2EC5E7B8]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__2FBA0BF1]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__30AE302A]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__31A25463]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3296789C]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__338A9CD5]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__347EC10E]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__3572E547]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__36670980]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__375B2DB9]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__384F51F2]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3943762B]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3A379A64]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3B2BBE9D]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__3C1FE2D6]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__3D14070F]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__3E082B48]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__3EFC4F81]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__3FF073BA]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__40E497F3]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__41D8BC2C]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__42CCE065]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__43C1049E]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__44B528D7]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__45A94D10]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__469D7149]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__47919582]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4885B9BB]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4979DDF4]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4A6E022D]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4B622666]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__4C564A9F]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4D4A6ED8]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__4E3E9311]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__4F32B74A]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__5026DB83]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__511AFFBC]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__520F23F5]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__5303482E]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__53F76C67]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__54EB90A0]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__55DFB4D9]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__56D3D912]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__57C7FD4B]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__58BC2184]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__59B045BD]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__5AA469F6]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__5B988E2F]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__5C8CB268]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__5D80D6A1]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_te__5E74FADA]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__5F691F13]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__605D434C]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__61516785]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__62458BBE]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__6339AFF7]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__642DD430]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__6521F869]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__66161CA2]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__670A40DB]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__67FE6514]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__68F2894D]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__69E6AD86]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__6ADAD1BF]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__6BCEF5F8]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__6C040022]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__6CC31A31]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__6CF8245B]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__6DB73E6A]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__6DEC4894]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__6EAB62A3]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__6EE06CCD]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__6F9F86DC]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_og__6FD49106]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__7093AB15]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ma__70C8B53F]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__7187CF4E]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__71BCD978]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__727BF387]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_da__72B0FDB1]
	DEFAULT ('01-01-1900') FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__737017C0]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__73A521EA]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__74643BF9]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__74994623]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__75586032]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__758D6A5C]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__764C846B]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_cu__76818E95]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__7740A8A4]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7775B2CE]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__7834CCDD]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7869D707]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__7928F116]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__795DFB40]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__7A1D154F]
	DEFAULT (getdate()) FOR [cd_lookup_created_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7A521F79]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__7B113988]
	DEFAULT (getdate()) FOR [cd_lookup_updated_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7B4643B2]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__7C055DC1]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redirection]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7C3A67EB]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__7CF981FA]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redelivery]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7D2E8C24]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7E22B05D]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7F16D496]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_consignment_p_cd_connote]
	ON [dbo].[edi_consignment] ([cd_connote])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [scheme_edi_consignment] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nci_wi_edi_consignment_00F67BFABF38D2B72A3BFFEB188998CA]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redirection], [cd_locked_for_editing_after_redelivery])
	INCLUDE ([cd_account], [cd_connote], [cd_consignment_date], [cd_deadweight], [cd_delivery_addr0], [cd_delivery_addr1], [cd_delivery_addr2], [cd_delivery_addr3], [cd_delivery_contact], [cd_delivery_contact_phone], [cd_delivery_email], [cd_delivery_postcode], [cd_delivery_suburb], [cd_insurance], [cd_items], [cd_pickup_addr0], [cd_pickup_addr1], [cd_pickup_addr2], [cd_pickup_addr3], [cd_pickup_contact], [cd_pickup_contact_phone], [cd_pickup_postcode], [cd_pickup_suburb], [cd_pricecode], [cd_special_instructions], [cd_volume])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [scheme_edi_consignment] ([cd_date])
GO
ALTER TABLE [dbo].[edi_consignment] SET (LOCK_ESCALATION = TABLE)
GO
