SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_incremental_consignment] (
		[id]                               [int] IDENTITY(1, 1) NOT NULL,
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL,
		[cd_delivery_status_flag]          [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__edi_incr__3213E83F7E727F90]
		PRIMARY KEY
		CLUSTERED
		([id])
)
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__00750D23]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__0169315C]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__025D5595]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_it__035179CE]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__04459E07]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__0539C240]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ra__062DE679]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ch__07220AB2]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__08162EEB]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__090A5324]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__09FE775D]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__0AF29B96]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_vo__0BE6BFCF]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_vo__0CDAE408]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__0DCF0841]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__0EC32C7A]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_me__0FB750B3]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_me__10AB74EC]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_bi__119F9925]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_bi__1293BD5E]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ex__1387E197]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ex__147C05D0]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__15702A09]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__16644E42]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__1758727B]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__184C96B4]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__1940BAED]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__1A34DF26]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__1B29035F]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__1C1D2798]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__1D114BD1]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__1E05700A]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__1EF99443]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__1FEDB87C]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__20E1DCB5]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__21D600EE]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__22CA2527]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__23BE4960]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__24B26D99]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__25A691D2]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__269AB60B]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__278EDA44]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__2882FE7D]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__297722B6]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__2A6B46EF]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__2B5F6B28]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__2C538F61]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__2D47B39A]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__2E3BD7D3]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__2F2FFC0C]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__30242045]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pr__3118447E]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__320C68B7]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__33008CF0]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_re__33F4B129]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pr__34E8D562]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__35DCF99B]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__36D11DD4]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__37C5420D]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__38B96646]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__39AD8A7F]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__3AA1AEB8]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__3B95D2F1]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__3C89F72A]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__3D7E1B63]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__3E723F9C]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__3F6663D5]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ca__405A880E]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_te__414EAC47]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_di__4242D080]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__4336F4B9]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__442B18F2]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__451F3D2B]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__46136164]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__4707859D]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__47FBA9D6]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__48EFCE0F]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__49E3F248]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__4AD81681]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__4BCC3ABA]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__4CC05EF3]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__4DB4832C]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__4EA8A765]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__4EDDB18F]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__4F9CCB9E]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__4FD1D5C8]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__5090EFD7]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ag__50C5FA01]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__51851410]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_im__51BA1E3A]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ac__52793849]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_og__52AE4273]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__536D5C82]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_ma__53A266AC]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__546180BB]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__54968AE5]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_fa__5555A4F4]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_da__558AAF1E]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_in__5649C92D]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_co__567ED357]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_la__573DED66]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_et__5772F790]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5832119F]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_et__58671BC9]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__592635D8]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_cu__595B4002]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__5A1A5A11]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5A4F643B]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_to__5B0E7E4A]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5B438874]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_tr__5C02A283]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5C37ACAD]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5D2BD0E6]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5E1FF51F]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__5F141958]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__60083D91]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__60FC61CA]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__61F08603]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__62E4AA3C]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__63D8CE75]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__64CCF2AE]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__65C116E7]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__66B53B20]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__67A95F59]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__689D8392]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__6991A7CB]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__6A85CC04]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__6B79F03D]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__6C6E1476]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__6D6238AF]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__6E565CE8]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_st__6F4A8121]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_st__703EA55A]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7132C993]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7226EDCC]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__731B1205]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__740F363E]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_sp__75035A77]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__75F77EB0]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__76EBA2E9]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__77DFC722]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__78D3EB5B]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__79C80F94]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7ABC33CD]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__7BB05806]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__7CA47C3F]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7D98A078]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_pi__7E8CC4B1]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__edi_incre__cd_de__7F80E8EA]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
