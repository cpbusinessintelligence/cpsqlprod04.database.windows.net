SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_edi_incremental_consignment] (
		[cd_id]                            [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_state_name]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_country]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_items]                         [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_status_flag]          [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_redelivery_type]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]     [datetime] NULL,
		CONSTRAINT [PK__redelive__D551B536E3AF535A]
		PRIMARY KEY
		CLUSTERED
		([cd_id])
)
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_ac__0ABD916C]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_co__0BB1B5A5]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_da__0CA5D9DE]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_co__0D99FE17]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__0E8E2250]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__0F824689]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__10766AC2]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__116A8EFB]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__125EB334]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__1352D76D]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__1446FBA6]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pi__153B1FDF]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_sp__162F4418]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_it__17236851]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_de__18178C8A]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_vo__190BB0C3]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_pr__19FFD4FC]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redeliver__cd_in__1AF3F935]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[redelivery_edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
