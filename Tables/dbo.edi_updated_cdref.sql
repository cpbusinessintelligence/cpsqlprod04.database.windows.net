SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_updated_cdref] (
		[cr_id]              [int] NOT NULL,
		[cr_company_id]      [int] NOT NULL,
		[cr_consignment]     [int] NOT NULL,
		[cr_reference]       [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__edi_upda__AB69D8CFC38E1E86]
		PRIMARY KEY
		CLUSTERED
		([cr_id])
)
GO
ALTER TABLE [dbo].[edi_updated_cdref]
	ADD
	CONSTRAINT [DF__edi_updat__cr_co__06ED0088]
	DEFAULT ('0') FOR [cr_company_id]
GO
ALTER TABLE [dbo].[edi_updated_cdref]
	ADD
	CONSTRAINT [DF__edi_updat__cr_co__07E124C1]
	DEFAULT ('0') FOR [cr_consignment]
GO
ALTER TABLE [dbo].[edi_updated_cdref]
	ADD
	CONSTRAINT [DF__edi_updat__cr_re__08D548FA]
	DEFAULT ('') FOR [cr_reference]
GO
ALTER TABLE [dbo].[edi_updated_cdref] SET (LOCK_ESCALATION = TABLE)
GO
