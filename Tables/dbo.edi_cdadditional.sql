SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdadditional] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__edi_cdad__64D0172949B36070]
		PRIMARY KEY
		CLUSTERED
		([ca_consignment])
	WITH (STATISTICS_NORECOMPUTE = ON)
)
GO
ALTER TABLE [dbo].[edi_cdadditional]
	ADD
	CONSTRAINT [DF__edi_cdadd__ca_at__02084FDA]
	DEFAULT ('N') FOR [ca_atl]
GO
ALTER TABLE [dbo].[edi_cdadditional]
	ADD
	CONSTRAINT [DF__edi_cdadd__ca_dg__02FC7413]
	DEFAULT ('N') FOR [ca_dg]
GO
CREATE NONCLUSTERED INDEX [nc_idx_cdadditional_lookup]
	ON [dbo].[edi_cdadditional] ([ca_consignment])
	INCLUDE ([ca_atl], [ca_dg])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_cdadditional] SET (LOCK_ESCALATION = TABLE)
GO
