SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WEBSITE_UPDATE_ITEM_LABEL]	

AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblItemLabel]
	SET	
			[ConsignmentID] = UPD.ConsignmentID,
			[LabelNumber] = UPD.LabelNumber,
			[Length] = UPD.[Length],
			[Width] = UPD.Width,
			[Height] = UPD.Height,
			[CubicWeight] = UPD.CubicWeight,
			[MeasureLength] = UPD.MeasureLength,
			[MeasureWidth] = UPD.MeasureWidth,
			[MeasureHeight] = UPD.MeasureHeight,
			[MeasureCubicWeight] = UPD.MeasureCubicWeight,
			[PhysicalWeight] = UPD.PhysicalWeight,
			[MeasureWeight] = UPD.MeasureWeight,
			[DeclareVolume] = UPD.DeclareVolume,
			[LastActivity] = UPD.LastActivity,
			[LastActivityDateTime] = UPD.LastActivityDateTime,
			[UnitValue] = UPD.UnitValue,
			[Quantity] = UPD.Quantity,
			[CountryOfOrigin] = UPD.CountryOfOrigin,
			[Description] = UPD.[Description],
			[HSTariffNumber] = UPD.HSTariffNumber,
			[CreatedDateTime] = UPD.CreatedDateTime,
			[CreatedBy] = UPD.CreatedBy,
			[UpdatedDateTime] = UPD.UpdatedDateTime,
			[UpdatedBy] = UPD.UpdatedBy,
			[DHLBarCode] = UPD.DHLBarCode
		FROM 
			[website_tblItemLabel] LBL WITH (NOLOCK)
		INNER JOIN
			[website_updated_tblItemLabel] UPD WITH (NOLOCK)
		ON
			LBL.[ItemLabelID] = UPD.[ItemLabelID]
END
GO
