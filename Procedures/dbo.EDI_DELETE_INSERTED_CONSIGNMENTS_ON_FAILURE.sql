SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[EDI_DELETE_INSERTED_CONSIGNMENTS_ON_FAILURE]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DELETE CON 
	FROM [edi_consignment] CON WITH (NOLOCK)
		INNER JOIN
	[edi_incremental_consignment] INC WITH (NOLOCK)
		ON
			CON.[cd_id] = INC.[cd_id]
		WHERE 
			INC.[cd_delivery_status_flag] = 'IN'
			AND
			CON.[cd_locked_for_editing_after_redirection] = 0
			AND
			CON.[cd_locked_for_editing_after_redelivery] = 0
END
GO
