SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WEBSITE_GET_CONSIGNMENT_JSON] 

AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		ConsignmentNumber, 		
		JSONString,
		SRC.[SourceID]
		FROM
		(SELECT 
			ConsignmentID, 
			ConsignmentCode AS ConsignmentNumber, 			
			(SELECT 
				CON.ConsignmentCode AS CONSIGNMENTNUMBER,
				CONVERT(DATE, CON.CreatedDateTime) AS CONSIGNMENTDATE,
				CONVERT(DATE, CON.CreatedDateTime) AS CREATEDDATE,	
				ISNULL(CON.RateCardID, '') AS SERVICECODE,
				ISNULL(CON.AccountNumber, '') AS ACCOUNTNUMBER,
				ISNULL(PickupCompanyName, '') AS SENDERCOMPANYNAME,
				[dbo].GET_FULL_NAME(PickupFirstName, PickupLastName) AS SENDERNAME,
				ISNULL(PickupAddress1, '') AS SENDERADDRESS1,
				ISNULL(PickupAddress2, '') AS SENDERADDRESS2,
				'' AS SENDERADDRESS3,
				ISNULL(PickupSuburb, '') AS SENDERLOCATION,					
				ISNULL(ISNULL([dbo].[WEBSITE_GET_STATE](PickupStateID), PickupStateName), '') AS SENDERSTATE,
				CONVERT(VARCHAR, PickupPostCode) AS SENDERPOSTCODE,
				ISNULL(PickupCountry, '') AS SENDERCOUNTRY,
				ISNULL(DestinationCompanyName, '') AS RECEIVERCOMPANYNAME,
				[dbo].GET_FULL_NAME(DestinationFirstName, DestinationLastName) AS RECEIVERNAME,
				ISNULL(DestinationAddress1, '') AS RECEIVERADDRESS1,
				ISNULL(DestinationAddress2, '') AS RECEIVERADDRESS2,
				'' AS RECEIVERADDRESS3,
				ISNULL(DestinationSuburb, '') AS RECEIVERLOCATION,				
				ISNULL(ISNULL([dbo].[WEBSITE_GET_STATE](DestinationStateID), DestinationStateName), '') AS RECEIVERSTATE,
				CONVERT(VARCHAR, DestinationPostCode) AS RECEIVERPOSTCODE,
				ISNULL(DestinationCountry, '') AS RECEIVERCOUNTRY,
				[dbo].GET_FULL_NAME(DestinationFirstName, DestinationLastName) AS RECEIVERCONTACT,
				ISNULL(DestinationPhone, '') AS RECEIVERPHONE,
				ISNULL(CON.[SpecialInstruction], '') AS SPECIALINSTRUCTIONS,
				CONVERT(VARCHAR, CON.NoOfItems) AS TOTALLOGISTICSUNITS,
				CONVERT(VARCHAR, CON.TotalWeight) AS TOTALDEADWEIGHT,
				CONVERT(VARCHAR, CASE WHEN CON.IsInternational = 0 THEN CON.TotalVolume/250 ELSE CON.TotalVolume/200 END) AS TOTALVOLUME,
				ISNULL(CON.InsuranceCategory,'') AS INSURANCECATEGORY,
				IIF(CON.[DangerousGoods] = 1, 'Y','N') AS DANGEROUSGOODSFLAG,
				[dbo].GET_FULL_NAME(PickupFirstName, PickupLastName) AS PICKUPCONTACT,
				ISNULL(PickupPhone, '') AS PICKUPPHONE,
				IIF(CON.[IsATl] = 1, 'Y','N') AS ATLFLAG,
				[dbo].[GET_FREIGHTCATEGORY](CON.RateCardID) AS FREIGHTCATEGORY,
				'' AS ALERT,
				ISNULL(CON.Delivery_Status_Flag, '') AS DELIVERYSTATUSFLAG,
				'Normal' AS DELIVERYTYPE,
				'' AS RESCHEDULEDDELIVERYDATE,
				IIF(([ConsignmentCode] like 'CPWSAV%' OR [ConsignmentCode] like 'CPWEXP%'),'Y','N') AS ISINTL,
				(SELECT 
					LBL.LabelNumber AS LABELNUMBER,
					'CTN' AS UNITTYPE,
					'1' AS INTERNALLABELNUMBER
					FROM [website_tblItemLabel] LBL WITH (NOLOCK) 
					WHERE CON.ConsignmentID = LBL.ConsignmentID FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'LABELS',
				(SELECT 
					ISNULL(CHILDCON.CustomerRefNo,'') AS ADDITIONALREFERENCE
					FROM [website_incremental_tblConsignment] CHILDCON WITH (NOLOCK) 
					WHERE CON.ConsignmentID = CHILDCON.ConsignmentID
					FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'REFERENCES'
			FROM 
				[website_incremental_tblConsignment] CON WITH (NOLOCK) 	
			WHERE 
				CON.ConsignmentID = RelationalJSONData.ConsignmentID
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			) JSONString
			FROM [website_incremental_tblConsignment] AS RelationalJSONData WITH (NOLOCK) 
		) AS JSONOnly
		LEFT JOIN [dbo].[outgoing_consignment_event_source] SRC WITH (NOLOCK) 
		ON SRC.[SourceName] = 'Website'
END
GO
