SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[REDELIVERY_UPDATE_EDI_CONSIGNMENT]
	
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION


		-- [edi_cdadditional]
		UPDATE [edi_cdadditional] 
		SET
			ca_atl = 'Y'
		FROM 
			[edi_cdadditional] CDADD WITH (NOLOCK)
		INNER JOIN
			[edi_consignment] CON WITH (NOLOCK)
		ON
			CDADD.ca_consignment = CON.[cd_id]
		INNER JOIN
			[redelivery_staging_tblRedelivery] RED WITH (NOLOCK)
		ON 
			RED.ConsignmentCode = CON.cd_connote		
		WHERE 
			CON.cd_locked_for_editing_after_redelivery = 0
			AND
			RED.[RedeliveryType] LIKE 'Deliver without%'
			AND
			RED.ConsignmentCode NOT LIKE ('CPW%')
			AND
			RED.ConsignmentCode NOT LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]';

		-- [edi_consignment]
		UPDATE [edi_consignment]
				SET	
					[cd_pickup_state_name] = ([dbo].[EDI_GET_STATE](cd_pickup_branch)),
					[cd_new_delivery_addr0] = (SELECT CONTENT FROM [SPLIT_STRING](RED.[DestinationAddress],100) WHERE [SEQUENCE] = 1),
					[cd_new_delivery_addr1] = (SELECT CONTENT FROM [SPLIT_STRING](RED.[DestinationAddress],100) WHERE [SEQUENCE] = 2),
					[cd_new_delivery_email] = RED.[Email],
					[cd_new_delivery_suburb] = RED.DestinationSuburb,
					[cd_new_delivery_state_name] = ISNULL([dbo].[WEBSITE_GET_STATE](RED.[State]), ''),
					[cd_new_delivery_postcode] = RED.DestinationPostCode,
					[cd_new_delivery_contact] = [dbo].GET_FULL_NAME(RED.Firstname, RED.LastName),
					[cd_new_delivery_contact_phone] = RED.PhoneNumber,
					[cd_rescheduled_delivery_date] = CONVERT(DATETIME, RED.SelectedETADate),
					[cd_special_instructions] = CAST(RED.SPInstruction AS VARCHAR(64)),
					[cd_lookup_updated_stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
					[cd_locked_for_editing_after_redelivery] = 1
				OUTPUT 
					INSERTED.cd_id,
					INSERTED.cd_account,
					INSERTED.cd_connote,
					INSERTED.cd_date,
					INSERTED.cd_consignment_date,					
					INSERTED.cd_pickup_addr0,
					INSERTED.cd_pickup_addr1,
					INSERTED.cd_pickup_addr2,
					INSERTED.cd_pickup_addr3,
					INSERTED.cd_pickup_suburb,
					INSERTED.cd_pickup_state_name,
					INSERTED.cd_pickup_postcode,
					INSERTED.cd_pickup_contact,
					INSERTED.cd_pickup_contact_phone,
					INSERTED.[cd_new_delivery_addr0] AS [cd_delivery_addr0],
					INSERTED.[cd_new_delivery_addr1] AS [cd_delivery_addr1],
					INSERTED.[cd_new_delivery_addr2] AS [cd_delivery_addr2],
					INSERTED.[cd_new_delivery_addr3] AS [cd_delivery_addr3],
					INSERTED.[cd_new_delivery_email] AS [cd_delivery_email],
					INSERTED.[cd_new_delivery_suburb] AS [cd_delivery_suburb],
					INSERTED.[cd_new_delivery_state_name] AS [cd_delivery_state_name],
					INSERTED.[cd_new_delivery_postcode] AS [cd_delivery_postcode],
					INSERTED.[cd_new_delivery_country] AS [cd_delivery_country],
					INSERTED.[cd_new_delivery_contact] AS [cd_delivery_contact],
					INSERTED.[cd_new_delivery_contact_phone] AS [cd_delivery_contact_phone],
					INSERTED.[cd_special_instructions] AS [cd_special_instructions],
					INSERTED.cd_items,
					INSERTED.cd_deadweight,
					INSERTED.cd_volume,
					INSERTED.cd_pricecode,
					INSERTED.cd_insurance,
					'UP' AS cd_delivery_status_flag,
					RED.[RedeliveryType] AS [cd_redelivery_type],
					INSERTED.[cd_rescheduled_delivery_date] AS cd_rescheduled_delivery_date
				INTO 
					[redelivery_edi_incremental_consignment]
			FROM 
				[edi_consignment] CON WITH (NOLOCK)
			INNER JOIN
				[redelivery_staging_tblRedelivery] RED WITH (NOLOCK)
			ON 
				CON.cd_connote = RED.ConsignmentCode			
			WHERE 			
				CON.[cd_locked_for_editing_after_redelivery] = 0
				AND	
				RED.ConsignmentCode NOT LIKE ('CPW%')
				AND
				RED.ConsignmentCode NOT LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]';
	COMMIT;
END
GO
