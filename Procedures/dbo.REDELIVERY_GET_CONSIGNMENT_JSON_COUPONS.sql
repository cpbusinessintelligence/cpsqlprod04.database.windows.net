SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[REDELIVERY_GET_CONSIGNMENT_JSON_COUPONS] 
			@PickupAddress1 varchar(100),
			@PickupAddress2 varchar(100),
			@PickupSuburb varchar(100),
			@PickupStateName varchar(100),
			@PickupPostCode nvarchar(20),
			@PickupPhone nvarchar(20),
			@PickupCountry nvarchar(100)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		ConsignmentNumber, 		
		JSONString,
		SRC.[SourceID]
		FROM
		(SELECT 
			ConsignmentCode AS ConsignmentNumber, 			
				(SELECT 
					RDI.ConsignmentCode AS CONSIGNMENTNUMBER,
					'' AS CONSIGNMENTDATE,
					'' AS CREATEDDATE,	
					'' AS SERVICECODE,
					'' AS ACCOUNTNUMBER,
					'' AS SENDERCOMPANYNAME,
					ISNULL(RDS.SenderName,'') AS SENDERNAME,
					ISNULL(@PickupAddress1, '') AS SENDERADDRESS1,
					ISNULL(@PickupAddress2, '') AS SENDERADDRESS2,
					ISNULL(@PickupSuburb, '') AS SENDERLOCATION,					
					ISNULL(@PickupStateName, '') AS SENDERSTATE,
					ISNULL(@PickupPostCode, '') AS SENDERPOSTCODE,
					ISNULL(@PickupCountry, '') AS SENDERCOUNTRY,
					'' AS RECEIVERCOMPANYNAME,
					[dbo].GET_FULL_NAME(RDS.[Firstname], RDS.LastName) AS RECEIVERNAME,
					[dbo].GET_FULL_NAME(RDS.[Firstname], RDS.LastName) AS RECEIVERADDRESS1,
					ISNULL((SELECT CONTENT FROM [SPLIT_STRING](RDS.[DestinationAddress],100) WHERE [SEQUENCE] = 1), '') AS RECEIVERADDRESS2,
					ISNULL((SELECT CONTENT FROM [SPLIT_STRING](RDS.[DestinationAddress],100) WHERE [SEQUENCE] = 2), '') AS RECEIVERADDRESS3,
					ISNULL(RDS.DestinationSuburb, '') AS RECEIVERLOCATION,				
					ISNULL([dbo].[WEBSITE_GET_STATE](RDS.[State]), '') AS RECEIVERSTATE,
					CONVERT(VARCHAR, (RDS.[DestinationPostCode])) AS RECEIVERPOSTCODE,
					'' AS RECEIVERCOUNTRY,
					[dbo].GET_FULL_NAME(RDS.[Firstname], RDS.LastName) AS RECEIVERCONTACT,
					ISNULL(RDS.PhoneNumber, '') AS RECEIVERPHONE,
					ISNULL(RDS.[SPInstruction], '') AS SPECIALINSTRUCTIONS,					
					CONVERT(VARCHAR,RDS.[NumberOfItem]) AS TOTALLOGISTICSUNITS,
					'' AS TOTALDEADWEIGHT,
					'' AS TOTALVOLUME,
					'' AS INSURANCECATEGORY,
					'N' AS DANGEROUSGOODSFLAG,
					ISNULL(RDS.SenderName,'') AS PICKUPCONTACT,
					ISNULL(@PickupPhone, '') AS PICKUPPHONE,
					IIF(RDS.[RedeliveryType] LIKE 'Deliver without%', 'Y', 'N') AS ATLFLAG,
					[dbo].[GET_FREIGHTCATEGORY]('') AS FREIGHTCATEGORY,
					'Customer booked redelivery. ' + IIF(LEN(RDS.[RedeliveryType]) > 0, RDS.[RedeliveryType] + '.','') +  IIF(LEN(RDS.[SPInstruction]) > 0, ' ' + RDS.[SPInstruction], '') + IIF(LEN(RDS.[PhoneNumber]) > 0,' (PH: ' + RDS.[PhoneNumber] + ')', '') AS ALERT,
					'IN' AS DELIVERYSTATUSFLAG,
					'Redelivery' AS DELIVERYTYPE,
					ISNULL(CONVERT(VARCHAR, CONVERT(DATE,  RDS.SelectedETADate)), '') AS RESCHEDULEDDELIVERYDATE,
					IIF((RDI.ConsignmentCode like 'CPWSAV%' OR RDI.ConsignmentCode like 'CPWEXP%'),'Y','N') AS ISINTL,
					(SELECT 
						CHILD_RDS1.LableNumber AS LABELNUMBER,	
						'' AS UNITTYPE,
						'' AS INTERNALLABELNUMBER
						FROM [redelivery_staging_tblRedelivery] CHILD_RDS1 WITH (NOLOCK) 
						WHERE CHILD_RDS1.ConsignmentCode = RDI.ConsignmentCode FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'LABELS',
					(SELECT 
						CHILD_RDS2.CardReferenceNumber AS ADDITIONALREFERENCE
						FROM [redelivery_staging_tblRedelivery] CHILD_RDS2 WITH (NOLOCK) 
						WHERE CHILD_RDS2.ConsignmentCode = RDI.ConsignmentCode 
						FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'REFERENCES'
				FROM 
					[redelivery_incremental_coupons] RDI WITH (NOLOCK) 
				INNER JOIN
					[redelivery_staging_tblRedelivery] RDS WITH (NOLOCK) 
				ON 
					RDI.ConsignmentCode = RDS.ConsignmentCode
				WHERE 
					RDI.ConsignmentCode = RelationalJSONData.ConsignmentCode
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)
			JSONString
			FROM [redelivery_incremental_coupons] AS RelationalJSONData  WITH (NOLOCK) 
		) AS JSONOnly
		LEFT JOIN [dbo].[outgoing_consignment_event_source] SRC WITH (NOLOCK) 
		ON SRC.[SourceName] = 'Coupon-Redeliver'
END
GO
