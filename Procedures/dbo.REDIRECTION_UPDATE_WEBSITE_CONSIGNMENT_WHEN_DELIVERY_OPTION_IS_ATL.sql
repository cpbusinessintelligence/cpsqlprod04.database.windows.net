SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[REDIRECTION_UPDATE_WEBSITE_CONSIGNMENT_WHEN_DELIVERY_OPTION_IS_ATL]
	
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblConsignment]
	SET	
		[SpecialInstruction] = RED.SpecialInstruction,	
		[IsATl] = RED.ATL,
		[Lookup_Updated_Stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
		[Locked_For_Editing_After_Redirection] = 1
			OUTPUT
				 INSERTED.ConsignmentID
				,INSERTED.ConsignmentCode
				,INSERTED.AccountNumber
				,INSERTED.PickupFirstName
				,INSERTED.PickupLastName
				,INSERTED.PickupCompanyName
				,INSERTED.PickupEmail
				,INSERTED.PickupAddress1
				,INSERTED.PickupAddress2
				,INSERTED.PickupSuburb
				,INSERTED.PickupStateName
				,INSERTED.PickupStateID
				,INSERTED.PickupPostCode
				,INSERTED.PickupPhone
				,INSERTED.PickupCountry
				,INSERTED.DestinationFirstName
				,INSERTED.DestinationLastName
				,INSERTED.DestinationCompanyName
				,INSERTED.DestinationEmail
				,INSERTED.DestinationAddress1
				,INSERTED.DestinationAddress2
				,INSERTED.DestinationSuburb
				,INSERTED.DestinationStateName
				,INSERTED.DestinationStateID
				,INSERTED.DestinationPostCode
				,INSERTED.DestinationPhone
				,INSERTED.DestinationCountry
				,INSERTED.TotalWeight
				,INSERTED.TotalVolume
				,INSERTED.NoOfItems
				,INSERTED.SpecialInstruction
				,INSERTED.DangerousGoods
				,INSERTED.RateCardID
				,INSERTED.CreatedDateTime
				,INSERTED.IsInternational
				,INSERTED.IsATl
				,INSERTED.InsuranceCategory
				,'UP' AS Delivery_Status_Flag
				,RED.SelectedDeliveryOption AS RedirectedDeliveryOption
				,NULL AS RescheduledDeliveryDate
			INTO
				[redirection_website_incremental_tblConsignment]
		FROM 
			[website_tblConsignment] CON  WITH (NOLOCK)
		INNER JOIN
				[redirection_staging_tblRedirectedConsignment] RED WITH (NOLOCK)
			ON 
				CON.ConsignmentCode = RED.ConsignmentCode			
			WHERE 			
				CON.[Locked_For_Editing_After_Redirection] = 0
				AND
				RED.SelectedDeliveryOption LIKE 'Authority%'
				AND
				RED.ConsignmentCode LIKE ('CPW%');
END
GO
