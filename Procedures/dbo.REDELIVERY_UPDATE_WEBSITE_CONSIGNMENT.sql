SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[REDELIVERY_UPDATE_WEBSITE_CONSIGNMENT]

AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblConsignment]
	SET	
		[NewDestinationFirstName] = CAST(RED.[Firstname] AS VARCHAR(50)),
		[NewDestinationLastName] = CAST(RED.[Lastname] AS VARCHAR(50)),
		[NewDestinationEmail] = CONVERT(VARCHAR, RED.Email),
		[NewDestinationAddress1] = (SELECT CONTENT FROM [SPLIT_STRING](RED.[DestinationAddress],100) WHERE [SEQUENCE] = 1),
		[NewDestinationAddress2] = (SELECT CONTENT FROM [SPLIT_STRING](RED.[DestinationAddress],100) WHERE [SEQUENCE] = 2),
		[NewDestinationSuburb] = RED.[DestinationSuburb],
		[NewDestinationStateName] = ISNULL([dbo].[WEBSITE_GET_STATE](RED.[State]), ''),
		[NewDestinationPostCode] = CONVERT(VARCHAR, (RED.[DestinationPostCode])),
		[NewDestinationPhone] = RED.PhoneNumber,
		[SpecialInstruction] = CAST(RED.[SPInstruction] AS VARCHAR(500)),	
		[IsATl] = IIF(RED.[RedeliveryType] LIKE 'Deliver without%', 1, [IsATl]),
		[RescheduledDeliveryDate] = CONVERT(DATETIME, RED.SelectedETADate),
		[Lookup_Updated_Stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
		[Locked_For_Editing_After_Redelivery] = 1
			OUTPUT
				 INSERTED.[ConsignmentID]
				,INSERTED.ConsignmentCode
				,INSERTED.AccountNumber
				,INSERTED.PickupFirstName
				,INSERTED.PickupLastName
				,INSERTED.PickupCompanyName
				,INSERTED.PickupEmail
				,INSERTED.PickupAddress1
				,INSERTED.PickupAddress2
				,INSERTED.PickupSuburb
				,INSERTED.PickupStateName
				,INSERTED.PickupStateID
				,INSERTED.PickupPostCode
				,INSERTED.PickupPhone
				,INSERTED.PickupCountry
				,INSERTED.NewDestinationFirstName AS DestinationFirstName
				,INSERTED.NewDestinationLastName AS DestinationLastName
				,INSERTED.NewDestinationCompanyName AS DestinationCompanyName
				,INSERTED.NewDestinationEmail AS DestinationEmail
				,INSERTED.NewDestinationAddress1 AS DestinationAddress1
				,INSERTED.NewDestinationAddress2 AS DestinationAddress2
				,INSERTED.NewDestinationSuburb AS DestinationSuburb
				,INSERTED.NewDestinationStateName AS DestinationStateName
				,INSERTED.NewDestinationStateID AS DestinationStateID
				,INSERTED.NewDestinationPostCode AS DestinationPostCode
				,INSERTED.NewDestinationPhone AS DestinationPhone
				,INSERTED.NewDestinationCountry AS DestinationCountry
				,INSERTED.TotalWeight
				,INSERTED.TotalVolume
				,INSERTED.NoOfItems
				,INSERTED.SpecialInstruction
				,INSERTED.DangerousGoods
				,INSERTED.RateCardID
				,INSERTED.CreatedDateTime
				,INSERTED.IsInternational
				,INSERTED.IsATl
				,INSERTED.InsuranceCategory
				,'UP' AS Delivery_Status_Flag
				,RED.RedeliveryType AS RedeliveryType
				,INSERTED.[RescheduledDeliveryDate] AS RescheduledDeliveryDate
			INTO
				[redelivery_website_incremental_tblConsignment]
		FROM 
			[website_tblConsignment] CON  WITH (NOLOCK)
		INNER JOIN
			[redelivery_staging_tblRedelivery] RED  WITH (NOLOCK)
		ON 
			CON.ConsignmentCode = RED.ConsignmentCode			
		WHERE 			
			CON.[Locked_For_Editing_After_Redelivery] = 0
			AND
			RED.ConsignmentCode LIKE ('CPW%')
			AND
			RED.ConsignmentCode NOT LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]';
END
GO
